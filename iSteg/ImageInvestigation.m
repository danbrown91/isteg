//
//  ImageInvestigation.m
//  iSteg
//
//  Created by Daniel Brown on 25/11/2013.
//  Copyright (c) 2013 Daniel Brown. All rights reserved.
//

#import "ImageInvestigation.h"

@interface ImageInvestigation ()

@end

@implementation ImageInvestigation

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
