//
//  iStegLoadStegoImage.m
//  iSteg
//
//  Created by Daniel Brown on 25/11/2013.
//  Copyright (c) 2013 Daniel Brown. All rights reserved.
//

#import "iStegLoadStegoImage.h"

@interface iStegLoadStegoImage ()

@end

@implementation iStegLoadStegoImage


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(IBAction)loadImage:(id)sender{
  self.imagePicker = [[UIImagePickerController alloc] init];
  self.imagePicker.delegate = self;
  self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
  self.popover = [[UIPopoverController alloc ] initWithContentViewController:self.imagePicker];
  [self.popover presentPopoverFromRect:CGRectMake(/*x*/265, /*y*/465, /*width*/500, /*height*/250) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
  self.chosenImage = info[UIImagePickerControllerOriginalImage];
  [self.loadedImage setImage:self.chosenImage];
  [self.popover dismissPopoverAnimated:YES];
  [self.investigateImage setHidden:NO];
  
}

-(IBAction)Investigate:(id)sender{
  [self performSelector:@selector(segue)withObject:self.chosenImage];
}

-(void)segue{
  [self performSegueWithIdentifier:@"Investigate" sender:self];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
