//
//  iStegImageInvestigation.m
//  iSteg
//
//  Created by Daniel Brown on 25/11/2013.
//  Copyright (c) 2013 Daniel Brown. All rights reserved.
//

#import "iStegImageInvestigation.h"


@interface iStegImageInvestigation ()

@end

NSMutableData *bmpHeader;
NSMutableData *flipImageData;
NSMutableData *stegoImage;
NSMutableData *dataNoAlpha;

NSUInteger a;
size_t width  = 0;

int replacementValue = 0;
int value2 = 0;
int x = 0;
int y = 0;
int d = 0;

int valueAt8 = 128;
int valueAt7 = 64;
int valueAt6 = 32;
int valueAt5 = 16;
int valueAt4 = 8;
int valueAt3 = 4;
int valueAt2 = 2;
int valueAt1 = 1;

int bitPosAt1 = 0;
int bitPosAt2 = 1;
int bitPosAt3 = 2;
int bitPosAt4 = 3;
int bitPosAt5 = 4;
int bitPosAt6 = 5;
int bitPosAt7 = 6;
int bitPosAt8 = 7;



@implementation iStegImageInvestigation

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}


//-----------------------------------------CORRECT ORDER-------------------------------------------------//

-(IBAction)loadImage:(id)sender{
  self.imagePicker = [[UIImagePickerController alloc] init];
  self.imagePicker.delegate = self;
  self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
  self.popover = [[UIPopoverController alloc ] initWithContentViewController:self.imagePicker];
  [self.popover presentPopoverFromRect:CGRectMake(/*x*/265, /*y*/465, /*width*/500, /*height*/250) inView:self.view permittedArrowDirections:UIPopoverArrowDirectionDown animated:YES];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
  self.chosenImage = info[UIImagePickerControllerOriginalImage];
  [self.loadedImage setImage:self.chosenImage];
  [self.popover dismissPopoverAnimated:YES];
  NSLog(@"%@", info);
  [self createBitmapHeader];
  [self getImageData];
  
  [self.checkBoxButton8 setHidden:NO];
  [self.checkBoxButton7 setHidden:NO];
  [self.checkBoxButton6 setHidden:NO];
  [self.checkBoxButton5 setHidden:NO];
  [self.checkBoxButton4 setHidden:NO];
  [self.checkBoxButton3 setHidden:NO];
  [self.checkBoxButton2 setHidden:NO];
  [self.checkBoxButton1 setHidden:NO];
  
  checked8 = YES;
  checked7 = YES;
  checked6 = YES;
  checked5 = YES;
  checked4 = YES;
  checked3 = YES;
  checked2 = YES;
  checked1 = YES;
  
  show8 = YES;
  show7 = YES;
  
  [self.checkBoxButton8 setImage:[UIImage imageNamed:@"Checked.png"] forState:UIControlStateNormal];
  [self.checkBoxButton7 setImage:[UIImage imageNamed:@"Checked.png"] forState:UIControlStateNormal];
  [self.checkBoxButton6 setImage:[UIImage imageNamed:@"Checked.png"] forState:UIControlStateNormal];
  [self.checkBoxButton5 setImage:[UIImage imageNamed:@"Checked.png"] forState:UIControlStateNormal];
  [self.checkBoxButton4 setImage:[UIImage imageNamed:@"Checked.png"] forState:UIControlStateNormal];
  [self.checkBoxButton3 setImage:[UIImage imageNamed:@"Checked.png"] forState:UIControlStateNormal];
  [self.checkBoxButton2 setImage:[UIImage imageNamed:@"Checked.png"] forState:UIControlStateNormal];
  [self.checkBoxButton1 setImage:[UIImage imageNamed:@"Checked.png"] forState:UIControlStateNormal];

}

-(void)createBitmapHeader{ //CREATES BITMAP HEADER AND STORES AS IMAGE.BMP FOR THE CHOSEN IMAGE.
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
  NSString *documentsDirectory = [paths objectAtIndex:0];
  UIImage *suspectImage = self.chosenImage;
  CGImageRef suspectCGImage = suspectImage.CGImage;
  width  = CGImageGetWidth(suspectCGImage);
  size_t height = CGImageGetHeight(suspectCGImage);
  int imageSize = 54 + (width*height*3);
  NSUInteger addLength = (width*height*3);
  NSString *filePathLoad =  [documentsDirectory stringByAppendingPathComponent:@"BMPHeaderEmpty"];
  NSString *filePathWrite =  [documentsDirectory stringByAppendingPathComponent:@"Image.bmp"];
  bmpHeader = [[NSMutableData alloc] initWithContentsOfFile:filePathLoad];
  for (int i = 0; i < [bmpHeader length]; i++) {
    if (i == 2) { //FILE SIZE
      const int replacementBytes[] = {imageSize};
      [bmpHeader replaceBytesInRange:NSMakeRange(i, 4) withBytes:replacementBytes];
    }
    if (i == 18) { //WIDTH
      const int replacementBytes[] = {width};
      [bmpHeader replaceBytesInRange:NSMakeRange(i, 4) withBytes:replacementBytes];
    }
    if (i == 22) { //HEIGHT
      const int replacementBytes[] = {height};
      [bmpHeader replaceBytesInRange:NSMakeRange(i, 4) withBytes:replacementBytes];
    }
    if (i == 28) { //BPP
      const int replacementBytes[] = {24};
      [bmpHeader replaceBytesInRange:NSMakeRange(i, 2) withBytes:replacementBytes];
    }
    if (i == 34) { //
      const int replacementBytes[] = {0};
      [bmpHeader replaceBytesInRange:NSMakeRange(i, 4) withBytes:replacementBytes];
    }
  }
  dataNoAlpha = [NSMutableData dataWithLength:addLength];
  [bmpHeader writeToFile:filePathWrite atomically:YES];
}

-(void)getImageData{
  UIImage *suspectImage = self.chosenImage;
  CGImageRef suspectCGImage = suspectImage.CGImage;
  CGDataProviderRef provider = CGImageGetDataProvider(suspectCGImage);
  self.suspectData = CFBridgingRelease(CGDataProviderCopyData(provider));
  x = 0;
  int skipAlpha = 3;
  for (int i = 0; i < [self.suspectData length]; i++) {
    if (i != skipAlpha){
      const uint8_t *databytes = [self.suspectData bytes];
      [self removeAlpha:(uint8_t)databytes[i]];
    }
    else {
      skipAlpha = skipAlpha + 4;
    }
  }
  NSMutableData *testData;
  testData = [[NSMutableData alloc] initWithData:dataNoAlpha];
  suspectDataHide = testData;
  suspectDataShow = testData;
  NSLog(@"END OF IMAGE");
  
  [self saveBitmap:dataNoAlpha];
}

-(void)removeAlpha:(int)value{
  const int bytes[] = {value};
  [dataNoAlpha replaceBytesInRange:NSMakeRange(x, 1) withBytes:bytes];
  x++;
}

///SAVE BITMAP PROCESS
-(void)saveBitmap:(NSData *)data{
  stegoImage = [NSMutableData dataWithData:bmpHeader];
  [stegoImage appendData:data];
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
  NSString *documentsDirectory = [paths objectAtIndex:0];
  NSString *filePathWrite =  [documentsDirectory stringByAppendingPathComponent:@"Image.bmp"];
  [stegoImage writeToFile:filePathWrite atomically:YES]; //AT THIS POINT THE IMAGE SHOULD BE VIEWABLE BUT ITS NOT.
  [self flipImage];
}

- (void)flipImage {
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
  NSString *documentsDirectory = [paths objectAtIndex:0];
  NSString *filePath =  [documentsDirectory stringByAppendingPathComponent:@"Image.bmp"];
  UIImage *suspectImage2 = [UIImage imageWithContentsOfFile:filePath];
  CGImageRef suspectCGImage2 = suspectImage2.CGImage;
  CGDataProviderRef provider = CGImageGetDataProvider(suspectCGImage2);
  flipImageData = CFBridgingRelease(CGDataProviderCopyData(provider));
  x = 54;
  int skipAlpha = 3;
  for (int i = 0; i < [flipImageData length]; i++) {
    if (i != skipAlpha){
      const uint8_t *databytes = [flipImageData bytes];
      [self insertData:(uint8_t)databytes[i]];
    }
    else {
     skipAlpha = skipAlpha + 4;
    }
  }
  NSLog(@"END OF IMAGE");
  [stegoImage writeToFile:filePath atomically:YES];
  suspectImage2 = [UIImage imageWithContentsOfFile:filePath];
  [self.loadedImage setImage:suspectImage2];
}

-(void)insertData:(int)value{
  const int bytes[] = {value};
  [stegoImage replaceBytesInRange:NSMakeRange(x, 1) withBytes:bytes]; //THIS IS WHERE THE DATA IS BEING ADDED TO BMPHEADER!!
  x++;
}
///SAVE BITMAP PROCESS END

//---------------------------------------CORRECT ORDER END-------------------------------------------------//

-(void)checkButton8:(id)sender {
  if (checked8){
    [self.checkBoxButton8 setImage:[UIImage imageNamed:@"Unchecked.png"] forState:UIControlStateNormal];
  checked8 = NO;
    bitPosition = bitPosAt8;
    bitPositionValue = valueAt8;
    
    valueAt7 = valueAt7 * 2;
    valueAt6 = valueAt6 * 2;
    valueAt5 = valueAt5 * 2;
    valueAt4 = valueAt4 * 2;
    valueAt3 = valueAt3 * 2;
    valueAt2 = valueAt2 * 2;
    valueAt1 = valueAt1 * 2;
    
    bitPosAt1 = bitPosAt1 + 1;
    bitPosAt2 = bitPosAt2 + 1;
    bitPosAt3 = bitPosAt3 + 1;
    bitPosAt4 = bitPosAt4 + 1;
    bitPosAt5 = bitPosAt5 + 1;
    bitPosAt6 = bitPosAt6 + 1;
    bitPosAt7 = bitPosAt7 + 1;
    [self hideBits];
  }
  else if (!checked8) {
    [self.checkBoxButton8 setImage:[UIImage imageNamed:@"Checked.png"] forState:UIControlStateNormal];
    checked8 = YES;
    bitPosition = 7;
    bitPositionValue = 128;
    [self showBits];
    

  }
}

-(void)checkButton7:(id)sender {
  if (checked7){
    [self.checkBoxButton7 setImage:[UIImage imageNamed:@"Unchecked.png"] forState:UIControlStateNormal];
    checked7 = NO;
    
    bitPosition = bitPosAt7;
    bitPositionValue = valueAt7;
    
    valueAt6 = valueAt6 * 2;
    valueAt5 = valueAt5 * 2;
    valueAt4 = valueAt4 * 2;
    valueAt3 = valueAt3 * 2;
    valueAt2 = valueAt2 * 2;
    valueAt1 = valueAt1 * 2;
    
    bitPosAt1 = bitPosAt1 + 1;
    bitPosAt2 = bitPosAt2 + 1;
    bitPosAt3 = bitPosAt3 + 1;
    bitPosAt4 = bitPosAt4 + 1;
    bitPosAt5 = bitPosAt5 + 1;
    bitPosAt6 = bitPosAt6 + 1;
    
    [self hideBits];
  }
  else if (!checked7) {
    [self.checkBoxButton7 setImage:[UIImage imageNamed:@"Checked.png"] forState:UIControlStateNormal];
    checked7 = YES;
    bitPosition = 6;
    bitPositionValue = 64;
    [self showBits];
  }
}

-(void)checkButton6:(id)sender {
  if (checked6){
    [self.checkBoxButton6 setImage:[UIImage imageNamed:@"Unchecked.png"] forState:UIControlStateNormal];
    checked6 = NO;
    
    
    bitPosition = bitPosAt6;
    bitPositionValue = valueAt6;

    valueAt5 = valueAt5 * 2;
    valueAt4 = valueAt4 * 2;
    valueAt3 = valueAt3 * 2;
    valueAt2 = valueAt2 * 2;
    valueAt1 = valueAt1 * 2;
    
    bitPosAt1 = bitPosAt1 + 1;
    bitPosAt2 = bitPosAt2 + 1;
    bitPosAt3 = bitPosAt3 + 1;
    bitPosAt4 = bitPosAt4 + 1;
    bitPosAt5 = bitPosAt5 + 1;
    
    [self hideBits];
  }
  else if (!checked6) {
    [self.checkBoxButton6 setImage:[UIImage imageNamed:@"Checked.png"] forState:UIControlStateNormal];
    checked6 = YES;
    bitPosition = 5;
    bitPositionValue = 32;
    [self showBits];

  }
}

-(void)checkButton5:(id)sender {
  if (checked5){
    [self.checkBoxButton5 setImage:[UIImage imageNamed:@"Unchecked.png"] forState:UIControlStateNormal];
    checked5 = NO;
    bitPosition = bitPosAt5;
    bitPositionValue = valueAt5;
    
    valueAt4 = valueAt4 * 2;
    valueAt3 = valueAt3 * 2;
    valueAt2 = valueAt2 * 2;
    valueAt1 = valueAt1 * 2;
    
    bitPosAt1 = bitPosAt1 + 1;
    bitPosAt2 = bitPosAt2 + 1;
    bitPosAt3 = bitPosAt3 + 1;
    bitPosAt4 = bitPosAt4 + 1;
    
    [self hideBits];
  }
  else if (!checked5) {
    [self.checkBoxButton5 setImage:[UIImage imageNamed:@"Checked.png"] forState:UIControlStateNormal];
    checked5 = YES;
    bitPosition = 4;
    bitPositionValue = 16;
    [self showBits];
  }
}

-(void)checkButton4:(id)sender {
  if (checked4){
    [self.checkBoxButton4 setImage:[UIImage imageNamed:@"Unchecked.png"] forState:UIControlStateNormal];
    checked4 = NO;
    bitPosition = bitPosAt4;
    bitPositionValue = valueAt4;
    
    valueAt3 = valueAt3 * 2;
    valueAt2 = valueAt2 * 2;
    valueAt1 = valueAt1 * 2;
    
    bitPosAt1 = bitPosAt1 + 1;
    bitPosAt2 = bitPosAt2 + 1;
    bitPosAt3 = bitPosAt3 + 1;
    
    [self hideBits];
  }
  else if (!checked4) {
    [self.checkBoxButton4 setImage:[UIImage imageNamed:@"Checked.png"] forState:UIControlStateNormal];
    checked4 = YES;
    bitPosition = 3;
    bitPositionValue = 8;
    [self showBits];
  }
}

-(void)checkButton3:(id)sender {
  if (checked3){
    [self.checkBoxButton3 setImage:[UIImage imageNamed:@"Unchecked.png"] forState:UIControlStateNormal];
    checked3 = NO;
    bitPosition = bitPosAt3;
    bitPositionValue = valueAt3;
    
    valueAt2 = valueAt2 * 2;
    valueAt1 = valueAt1 * 2;
    
    bitPosAt1 = bitPosAt1 + 1;
    bitPosAt2 = bitPosAt2 + 1;
    
    [self hideBits];
  }
  else if (!checked3) {
    [self.checkBoxButton3 setImage:[UIImage imageNamed:@"Checked.png"] forState:UIControlStateNormal];
    checked3 = YES;
    bitPosition = 2;
    bitPositionValue = 4;
    [self showBits];
  }
}

-(void)checkButton2:(id)sender {
  if (checked2){
    [self.checkBoxButton2 setImage:[UIImage imageNamed:@"Unchecked.png"] forState:UIControlStateNormal];
    checked2 = NO;
    bitPosition = bitPosAt2;
    bitPositionValue = valueAt2;
    bitPosAt1 = bitPosAt1 + 1;
    valueAt1 = valueAt1 * 2;
    [self hideBits];
  }
  else if (!checked2) {
    [self.checkBoxButton2 setImage:[UIImage imageNamed:@"Checked.png"] forState:UIControlStateNormal];
    checked2 = YES;
    bitPosition = 1;
    bitPositionValue = 2;
    [self showBits];
  }
}

-(void)checkButton1:(id)sender {
  if (checked1){
    [self.checkBoxButton1 setImage:[UIImage imageNamed:@"Unchecked.png"] forState:UIControlStateNormal];
    checked1 = NO;
    
    bitPosition = bitPosAt1;
    bitPositionValue = valueAt1;
        
    [self hideBits];
  }
  else if (!checked1) {
    [self.checkBoxButton1 setImage:[UIImage imageNamed:@"Checked.png"] forState:UIControlStateNormal];
    checked1 = YES;
    bitPosition = 0;
    bitPositionValue = 1;
    [self showBits];
  }
}

//STEPS THROUGH DATA, AND RUNDS METHOD BELOW. REPLACES BYTE VALUE WITH DECIMAL VALUE CALCULATED IN BELOW METHOD.
-(void)hideBits {

  for (int i = 0; i < [suspectDataHide length]; i++) {
      const uint8_t *databytes = [suspectDataHide bytes];
      [self removeBitValue:(uint8_t)databytes[i]];
      const int replacementBytes[] = {replacementValue};
      [suspectDataHide replaceBytesInRange:NSMakeRange(i, 1) withBytes:replacementBytes];
      replacementValue = 0;
}
  NSLog(@"END OF IMAGE");
  [self saveBitmap:suspectDataHide];
  suspectDataShow = suspectDataHide;
  //[self buildImage:suspectDataHide];
}

//CHECKS BIT POSITION THEN PERFORMS CALCULATION TO ALTER THE VALUE (EFFECTIVELY HIDING THE BIT) THEN CALLS THE METHOD TO CREATE DECIMAL VALUES FOR THE NEW DATA.
-(void)removeBitValue:(int)value {

  NSString *bits = @"";
  if (value != 0) {
    for (int i = 0; i < 8; i++) {
      if (i == bitPosition){
        bits = [NSString stringWithFormat:@"%i", value & (1 << i) ? 1 : 0];
        if ([bits isEqualToString:@"1"]) {
        value = value - bitPositionValue;
        }
        replacementValue = value;
      }
    }
  }
  for (int i = 0; i < bitPosition; i++) {
    bits = [NSString stringWithFormat:@"%i", value & (1 << i) ? 1 : 0];
    if ([bits isEqualToString:@"1"]) {
      [self shiftLeft:i];
    }
  }
}

-(void)shiftLeft:(int)i{

  switch (i) {
    case 0:
      replacementValue = replacementValue + 1;
      break;
    case 1:
      replacementValue = replacementValue + 2;
      break;
    case 2:
      replacementValue = replacementValue + 4;
      break;
    case 3:
      replacementValue = replacementValue + 8;
      break;
    case 4:
      replacementValue = replacementValue + 16;
      break;
    case 5:
      replacementValue = replacementValue + 32;
      break;
    case 6:
      replacementValue = replacementValue + 64;
      break;
    //case 7:
      //replacementValue = replacementValue + 2;
      //break;
      
    default:
      break;
  }
}


-(void)showBits{
  for (int i = 0; i < [suspectDataShow length]; i++) {
    const uint8_t *databytes = [suspectDataShow bytes];
    [self shiftRight:(uint8_t)databytes[i]];
      //replacementValue = databytes[i];
      //replacementValue = replacementValue - bitPositionValue;
    const uint8_t *databytes2 = [dataNoAlpha bytes];
    [self checkOriginal:(uint8_t)databytes2[i]];
    const int replacementBytes[] = {replacementValue};
    [suspectDataShow replaceBytesInRange:NSMakeRange(i, 1) withBytes:replacementBytes];
  }
  
  NSLog(@"END OF IMAGE");
  [self saveBitmap:suspectDataShow];
  suspectDataHide = suspectDataShow;
}

-(void)shiftRight:(int)value{
  replacementValue = value;
  NSString *bits = @"";
  for (int i = 0; i < 8; i++) {
    bits = [NSString stringWithFormat:@"%i", value & (1 << i) ? 1 : 0];
    if ([bits isEqualToString:@"1"]) {
      switch (i) {
        case 1:
                  replacementValue = replacementValue - 1;
        break;
        case 2:
                  replacementValue = replacementValue - 2;
        break;
        case 3:
                  replacementValue = replacementValue - 4;
        break;
        case 4:
                  replacementValue = replacementValue - 8;
        break;
        case 5:
                  replacementValue = replacementValue - 16;
        break;
        case 6:
                  replacementValue = replacementValue - 32;
        break;
        case 7:
                  replacementValue = replacementValue - 64;
        break;
      }
    }
  }
}

-(void)checkOriginal:(int)value{
  NSString *bits = @"";
  for (int i = 0; i < 8; i++) {
    if (i == bitPosition){
      bits = [NSString stringWithFormat:@"%i", value & (1 << i) ? 1 : 0];
      if ([bits isEqualToString:@"1"]) {
        replacementValue = replacementValue + 128;
        /*
        switch (i) {
          case 0:
            replacementValue = replacementValue + 1;
            break;
          case 1:
            replacementValue = replacementValue + 2;
            break;
          case 2:
            replacementValue = replacementValue + 4;
            break;
          case 3:
            replacementValue = replacementValue + 8;
            break;
          case 4:
            replacementValue = replacementValue + 16;
            break;
          case 5:
            replacementValue = replacementValue + 32;
            break;
          case 6:
            replacementValue = replacementValue + 64;
            break;
          case 7:
            replacementValue = replacementValue + 128;
            break;
        }
         //*/
      }
    }
  }
}








  
//BUILDS A UIIMAGE FROM THE DATA
/*
 -(void)buildImage:(NSData*)data{ //THIS IS THE ONE THAT SEEMS TO WORK PERFECTLY. OUTPUT IMAGE TYPE UNKNOWN.
  
  NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
  NSString *documentsDirectory = [paths objectAtIndex:0];
  NSString *filePath =  [documentsDirectory stringByAppendingPathComponent:@"file.bmp"];
  //NSError *error;
  [data writeToFile:filePath atomically:YES];
  
  //[data writeToFile:filePath atomically:NO encoding:NSUTF8StringEncoding error:&error];
  CGImageRef cgimage2 = self.chosenImage.CGImage;
  size_t width  = CGImageGetWidth(cgimage2);
  size_t height = CGImageGetHeight(cgimage2);
  size_t bpr = CGImageGetBytesPerRow(cgimage2);
  size_t bpp = CGImageGetBitsPerPixel(cgimage2);
  CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)(data));
  CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
  CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
  CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;
  CGImageRef imageRef = CGImageCreate(width, height, 8, bpp, bpr, colorSpaceRef, bitmapInfo, provider, NULL, NO, renderingIntent);
  UIImage *newImage = [UIImage imageWithCGImage:imageRef];
  CGColorSpaceRelease(colorSpaceRef);
  CGImageRelease(imageRef);
  [self.loadedImage setImage:newImage];

  for (int i = 0; i < [data length]; i++) {
    const uint8_t *databytes = [data bytes];
    [self getBitString:(uint8_t)databytes[i]];
  }
  NSLog(@"BUILT IMAGE");
}
*/
//BUILDS A UIIMAGE FROM THE DATA
/*
 -(void)buildImage2:(NSData*)data{
  
  CGImageRef cgimage2 = self.chosenImage.CGImage;
  size_t width  = CGImageGetWidth(cgimage2);
  size_t height = CGImageGetHeight(cgimage2);
  size_t bpr = CGImageGetBytesPerRow(cgimage2);
  //size_t bpp = CGImageGetBitsPerPixel(cgimage2);
  CGColorSpaceRef colorSpaceRef = CGImageGetColorSpace(cgimage2);
  CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
  NSMutableData *d;
  [d setLength:bpr * height];
  CGContextRef bitmap = CGBitmapContextCreateWithData((__bridge void *)(d), width, height, 8, bpr, colorSpaceRef, bitmapInfo, NULL, NULL);
  CGImageRef a = CGBitmapContextCreateImage(bitmap);
  UIImage *newImage = [UIImage imageWithCGImage:a];
  CGContextRelease(bitmap);
  CGImageRelease(a);
  [self.loadedImage setImage:newImage];
}
 */

@end
