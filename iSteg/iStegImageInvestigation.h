//
//  iStegImageInvestigation.h
//  iSteg
//
//  Created by Daniel Brown on 25/11/2013.
//  Copyright (c) 2013 Daniel Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface iStegImageInvestigation : UIViewController <UIImagePickerControllerDelegate, UINavigationBarDelegate, UINavigationControllerDelegate, UIPickerViewDataSource, UIPickerViewDelegate> {

  BOOL checked8;
  BOOL checked7;
  BOOL checked6;
  BOOL checked5;
  BOOL checked4;
  BOOL checked3;
  BOOL checked2;
  BOOL checked1;
  
  BOOL show8;
  BOOL show7;
  
  int bitPosition;
  int bitPositionValue;
  NSMutableData *suspectDataHide;
  NSMutableData *suspectDataShow;
}

@property (weak, nonatomic) IBOutlet UIImageView *loadedImage;
@property (strong, nonatomic) UIImagePickerController *imagePicker;
@property (strong, nonatomic) UIPopoverController *popover;
@property (strong, nonatomic) UIImage *chosenImage;
@property (weak, nonatomic) IBOutlet UIButton *loadImage;
@property (weak, nonatomic) IBOutlet UIButton *investigateImage;
@property (strong, nonatomic) NSMutableData *suspectData;
@property (weak, nonatomic) IBOutlet UIButton *bmpHeader;
@property (weak, nonatomic) IBOutlet UIButton *checkBoxButton8;
@property (weak, nonatomic) IBOutlet UIButton *checkBoxButton7;
@property (weak, nonatomic) IBOutlet UIButton *checkBoxButton6;
@property (weak, nonatomic) IBOutlet UIButton *checkBoxButton5;
@property (weak, nonatomic) IBOutlet UIButton *checkBoxButton4;
@property (weak, nonatomic) IBOutlet UIButton *checkBoxButton3;
@property (weak, nonatomic) IBOutlet UIButton *checkBoxButton2;
@property (weak, nonatomic) IBOutlet UIButton *checkBoxButton1;


-(IBAction)loadImage:(id)sender;
-(IBAction)checkButton8:(id)sender;
-(IBAction)checkButton7:(id)sender;
-(IBAction)checkButton6:(id)sender;
-(IBAction)checkButton5:(id)sender;
-(IBAction)checkButton4:(id)sender;
-(IBAction)checkButton3:(id)sender;
-(IBAction)checkButton2:(id)sender;
-(IBAction)checkButton1:(id)sender;
-(IBAction)bmpHeader:(id)sender;

@end