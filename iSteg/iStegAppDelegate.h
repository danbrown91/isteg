//
//  iStegAppDelegate.h
//  iSteg
//
//  Created by Daniel Brown on 25/11/2013.
//  Copyright (c) 2013 Daniel Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface iStegAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
