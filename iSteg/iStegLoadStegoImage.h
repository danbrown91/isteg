//
//  iStegLoadStegoImage.h
//  iSteg
//
//  Created by Daniel Brown on 25/11/2013.
//  Copyright (c) 2013 Daniel Brown. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface iStegLoadStegoImage : UIViewController <UIImagePickerControllerDelegate, UINavigationBarDelegate, UINavigationControllerDelegate> {

}

@property (weak, nonatomic) IBOutlet UIImageView *loadedImage;
@property (strong, nonatomic) UIImagePickerController *imagePicker;
@property (strong, nonatomic) UIPopoverController *popover;
@property (strong, nonatomic) UIImage *chosenImage;
@property (weak, nonatomic) IBOutlet UIButton *loadImage;
@property (weak, nonatomic) IBOutlet UIButton *investigateImage;

-(IBAction)loadImage:(id)sender;
-(IBAction)Investigate:(id)sender;
@end
